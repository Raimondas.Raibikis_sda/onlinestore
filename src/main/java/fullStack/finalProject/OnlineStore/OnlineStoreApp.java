package fullStack.finalProject.OnlineStore;
// version 0.1
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineStoreApp {

	public static void main(String[] args) {
		SpringApplication.run(OnlineStoreApp.class, args);
	}

}
