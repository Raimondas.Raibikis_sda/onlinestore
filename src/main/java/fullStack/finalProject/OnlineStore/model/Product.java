package fullStack.finalProject.OnlineStore.model;
// version 0.1

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Product implements Serializable {
    enum ProductType {
        TELEVISIONS,
        HOMECINEMASYSTEMS,
        AUDIOSYSTEMS,
        ACCESSORIES,
        NOTDEFINED
    }
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private long productId; // unikalus produkto (prekės) kodas šiame projekte
    @Column(nullable = false, updatable = false)
    private long productNumber; // gamintojo produkto numeris
    private String title;  // prekės pavadinimas, modelis
    private String description;  // prekės aprašymas
    private String thumbnailUrl; // interneto adresas nuoroda į gamintojo prekės aprašymą?
    private String category;  // entity - prekės kategorija
    private float price; // prekės kaina EUR su PVM.
    // product type (enum)- apsirasyti, kaip enum tipo kintamajį
    // Optional for each product
    // https://support.google.com/merchants/answer/6324406?hl=en&ref_topic=6324338
    private ProductType productType = ProductType.NOTDEFINED;
    private String author; // entity

    public Product() {}

    public Product(long productNumber, String title, String description, String thumbnailUrl, String category, float price, ProductType productType, String author) {
        this.productNumber = productNumber;
        this.title = title;
        this.description = description;
        this.thumbnailUrl = thumbnailUrl;
        this.category = category;
        this.price = price;
        this.productType = productType;
        this.author = author;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", productNumber='" + productNumber + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", thumbnailUrl='" + thumbnailUrl + '\'' +
                ", category='" + category + '\'' +
                ", price='" + price + '\'' +
                ", productType='" + productType + '\'' +
                ", author='" + author + '\'' +
                '}';
    }


}
