# OnlineStore

Online Store app written in Java allows to add products to the online store via the administration panel. The system enables user registration and loggin as well as placing an order. A website is built using Spring Boot, JPA (Hibernate) and Angular.